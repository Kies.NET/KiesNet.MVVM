﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace KiesNet.MVVM.Behaviors
{
    public class FilterBehavior : Behavior<ListBox>
    {
        public static DependencyProperty SearchTextProperty =
            DependencyProperty.Register(nameof(SearchText), typeof(string), typeof(FilterBehavior), 
                new PropertyMetadata((dependencyObject, args) => ((FilterBehavior)dependencyObject).Filter()));

        /// <summary>
        /// Defines if filter properties should be used
        ///
        /// <para>
        ///     Works only with generic types
        /// </para>
        /// </summary>
        public bool UseFilterProperties { get; set; } = true;

        public string SearchText
        {
            get => (string) this.GetValue(SearchTextProperty);
            set => this.SetValue(SearchTextProperty, value);
        }

        private PropertyInfo[] m_filterProperties;

        protected override void OnAttached()
        {
            if (this.UseFilterProperties)
                this.RefreshFilterProperties();
        }

        private void Filter()
        {
            if (this.AssociatedObject?.ItemsSource == null)
                return;

            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.AssociatedObject?.ItemsSource);

            if (string.IsNullOrEmpty(this.SearchText))
                collectionView.Filter = null;

            else if (this.m_filterProperties?.Any() ?? false)
            {
                collectionView.Filter = obj => this.m_filterProperties.Any(property =>
                    this.FilterPropertyContainsValue(property, obj, this.SearchText));
            }

            else
                collectionView.Filter = obj => obj.ToString().Contains(this.SearchText);
        }

        private bool FilterPropertyContainsValue(PropertyInfo filterProperty, object obj, string value)
        {
            if (!filterProperty.PropertyType.IsReferenceType())
                return filterProperty.GetValue(obj)?.ToString().ContainsLower(value) ?? false;

            // Search for filter properties again
            IEnumerable<PropertyInfo> filterProperties;

            if (filterProperty.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
            {
                if (!filterProperty.PropertyType.IsGenericType 
                    && !filterProperty.PropertyType.IsArray)
                    throw new Exception("Collection has to be generic");

                Type genericType = filterProperty.PropertyType.IsGenericType
                                    ? filterProperty.PropertyType.GetTypeInfo().GetGenericArguments().First()
                                    : filterProperty.PropertyType.GetElementType();

                filterProperties = genericType.GetProperties()
                    .Where(info => info.GetCustomAttribute<FilterPropertyAttribute>() != null);

                IEnumerable<object> collection = ((IEnumerable) filterProperty.GetValue(obj)).Cast<object>();

                return genericType.IsReferenceType()
                    ? collection.Any(currentObject => filterProperties.Any(info => this.FilterPropertyContainsValue(info, currentObject, value)))
                    : collection.Any(currentObject => currentObject?.ToString().ContainsLower(value) ?? false);
            }
            
            filterProperties = filterProperty.PropertyType
                .GetProperties()
                .Where(info => info.GetCustomAttribute<FilterPropertyAttribute>() != null);

            return filterProperties.Any(property => this.FilterPropertyContainsValue(property, filterProperty.GetValue(obj), value));
            
        }

        private void RefreshFilterProperties()
        {
            if (this.AssociatedObject?.ItemsSource == null)
                return;

            Type itemsSourceType = this.AssociatedObject.ItemsSource.GetType();

            if (!itemsSourceType.IsGenericType
                    && !itemsSourceType.IsArray)
                return;

            Type genericType = itemsSourceType.IsGenericType 
                ? itemsSourceType.GetTypeInfo().GetGenericArguments().First()
                : itemsSourceType.GetElementType();
            
            this.m_filterProperties = genericType.GetProperties()
                .Where(propertyInfo => propertyInfo.GetCustomAttribute<FilterPropertyAttribute>() != null)
                .ToArray();
        }
    }
}
