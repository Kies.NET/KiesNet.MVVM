﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiesNet.MVVM
{
    public class Progress
    {
        public string Name { get; }
        public double Value { get; internal set; }
        public string Description { get; }

        public Progress(string name, string description = "")
        {
            this.Name = name;
            this.Description = description;
        }

        public override bool Equals(object obj)
        {
            return obj is Progress otherProgress && otherProgress.Name == this.Name;
        }

        public override int GetHashCode()
        {
            return this.Name != null ? this.Name.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
