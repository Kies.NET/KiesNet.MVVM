﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KiesNet.MVVM
{
    public class ServiceContainer
    {
        public static ServiceContainer Instance => s_instance ?? (s_instance = new ServiceContainer());

        private static ServiceContainer s_instance;

        private readonly List<Service> m_services = new List<Service>();

        private ServiceContainer()
        {
        }

        public T GetService<T>() where T : Service
        {
            T returnValue = this.m_services.OfType<T>().FirstOrDefault();

            if (returnValue == null)
            {
                returnValue = (T) Activator.CreateInstance(typeof(T), true);
                returnValue.OnInitialize();

                this.m_services.Add(returnValue);
            }

            return returnValue;
        }

        public void Destroy<T>() where T : Service
        {
            Service service = this.m_services.OfType<T>().FirstOrDefault();

            if (service == null)
                return;

            service.OnDestroy();
            this.m_services.Remove(service);
        }
    }
}