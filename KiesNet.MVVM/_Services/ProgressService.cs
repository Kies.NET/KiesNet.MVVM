﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KiesNet.MVVM
{
    public class ProgressService : Service
    {
        public event EventHandler<ProgressEventArgs> ProgressAdded;
        public event EventHandler<ProgressEventArgs> ProgressRemoved;
        public event EventHandler<ProgressEventArgs> ProgressValueChanged;

        private readonly List<Progress> m_progresses = new List<Progress>();

        public void AddProgress(Progress progress)
        {
            if (this.m_progresses.Contains(progress))
                throw new ArgumentException("Progress with the same name already existing");

            this.m_progresses.Add(progress);
            this.ProgressAdded?.Invoke(this, new ProgressEventArgs(progress));
        }

        public Progress GetProgress(string name)
        {
            Progress progress = this.m_progresses.FirstOrDefault(p => p.Name == name);

            if (progress == null)
                throw new ArgumentException($"Progress with name {name} not found");

            return progress;
        }

        public bool ProgressRunning(Progress progress)
        {
            return this.m_progresses.Contains(progress);
        }

        public void RemoveProgress(Progress progress)
        {
            if (!this.m_progresses.Contains(progress))
                throw new ArgumentException("Progress not found");

            this.m_progresses.Remove(progress);
            this.ProgressRemoved?.Invoke(this, new ProgressEventArgs(progress));
        }

        public void UpdateProgressValue(Progress progress, double value)
        {
            if (!this.m_progresses.Contains(progress))
                throw new ArgumentNullException(nameof(progress), "Progress not found");

            progress.Value = value;
            this.ProgressValueChanged?.Invoke(this, new ProgressEventArgs(progress));
        }
    }
}
