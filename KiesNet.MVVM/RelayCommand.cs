﻿using System;
using System.Windows.Input;

namespace KiesNet.MVVM
{
    /// <summary>
    /// Baseclass for commands
    /// </summary>
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// Delegate, which is executed when running the command
        /// </summary>
        public Action<object> ExecuteAction { get; set; }

        /// <summary>
        /// Predicate, which controls when the ui element is
        /// enabled
        /// </summary>
        public Func<object, bool> CanExecutePredicate { get; set; }

        /// <summary>
        /// EventHandler, which communicates the the gui
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute)
        {
            this.ExecuteAction = execute;
            this.CanExecutePredicate = canExecute;
        }

        public RelayCommand(Action<object> execute) : this(execute, o => true)
        {
        }

        public RelayCommand(Action execute, Func<bool> canExecute) : this(obj => execute(), obj => canExecute())
        {
        }

        public RelayCommand(Action execute) : this(execute, () => true)
        {
        }

        /// <summary>
        /// Checks, if the element is allowed to call the command
        /// </summary>
        /// <param name="parameter">Value of command parameter</param>
        /// <returns></returns>
        public bool CanExecute(object parameter) => this.CanExecutePredicate(parameter);

        /// <summary>
        /// Executes the command
        /// </summary>
        /// <param name="parameter">Value of command parameter</param>
        public void Execute(object parameter) => this.ExecuteAction(parameter);
    }
}
