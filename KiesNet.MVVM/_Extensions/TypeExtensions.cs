﻿using System;

namespace KiesNet.MVVM
{
    internal static class TypeExtensions
    {
        internal static bool IsReferenceType(this Type type)
        {
            return type != typeof(string)
                   && type != typeof(sbyte)
                   && type != typeof(short)
                   && type != typeof(int)
                   && type != typeof(long)
                   && type != typeof(byte)
                   && type != typeof(ushort)
                   && type != typeof(uint)
                   && type != typeof(ulong)
                   && type != typeof(float)
                   && type != typeof(double)
                   && type != typeof(decimal);
        }
    }
}
