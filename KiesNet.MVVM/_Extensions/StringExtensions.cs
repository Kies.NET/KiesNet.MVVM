﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiesNet.MVVM
{
    internal static class StringExtensions
    {
        internal static bool ContainsLower(this string baseString, string value)
        {
            return baseString.ToLower().Contains(value.ToLower());
        }
    }
}
