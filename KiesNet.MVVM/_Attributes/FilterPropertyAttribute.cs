﻿using System;

namespace KiesNet.MVVM
{
    /// <summary>
    /// Uses this property when filtering with FilterBehavior
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class FilterPropertyAttribute : Attribute
    {
    }
}
