﻿using System;

namespace KiesNet.MVVM
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ResettableAttribute : Attribute
    {
        public object DefaultValue { get; set; }

        public ResettableAttribute(object defaultValue)
        {
            this.DefaultValue = defaultValue;
        }
    }
}
