﻿using System;

namespace KiesNet.MVVM
{
    public class ProgressEventArgs : EventArgs
    {
        public Progress Progress { get; }

        public ProgressEventArgs(Progress progress)
        {
            this.Progress = progress;
        }
    }
}
