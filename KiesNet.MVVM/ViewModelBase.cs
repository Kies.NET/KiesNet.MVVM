﻿using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace KiesNet.MVVM
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        
        protected void ResetPropertyValues()
        {
            Parallel.ForEach(this.GetType().GetProperties().Where(p => p.GetCustomAttribute<ResettableAttribute>() != null),
                propertyInfo =>
                {
                    ResettableAttribute resettableAttribute = propertyInfo.GetCustomAttribute<ResettableAttribute>();
                    
                    propertyInfo.SetValue(this, resettableAttribute.DefaultValue);
                });
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
